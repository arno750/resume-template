.DEFAULT_GOAL := build

FILE ?= cv

help: ## This help message
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\033[1;34m\1\\033[0;39m:\2/' | column -c2 -t -s :)"

clean: ## Clean
	rm -f *.out *aux *bbl *blg *log *toc *.ptb *.tod *.fls *latexmk *synctex.gz page*.png *dvi *ka *.lof message.json

clean/all: clean ## Clean all (including artefact)
	rm -f *.pdf

build: ## build the specified document
	xelatex ${FILE}.tex

build/fr: ## build the French document
	xelatex ${FILE}-fr.tex

build/en: ## build the English document
	xelatex ${FILE}-en.tex

build/all: clean/all build/fr buildf/en clean ## build all the documents

docker/fr: ## build the French document using docker
	docker run --rm --user $$(id -u):$$(id -g) -it -w "/doc" -v "${PWD}":/doc -e FILE="cv-fr" texlive/texlive make build
	rm -f *.out *aux *bbl *blg *log *toc *.ptb *.tod *.fls *latexmk *synctex.gz page*.png *dvi *ka *.lof

docker/en: ## build the English document using docker
	docker run --rm --user $$(id -u):$$(id -g) -it -w "/doc" -v "${PWD}":/doc -e FILE="cv-en" texlive/texlive make build
	rm -f *.out *aux *bbl *blg *log *toc *.ptb *.tod *.fls *latexmk *synctex.gz page*.png *dvi *ka *.lof

docker/all: clean/all docker/fr docker/en ## build all the documents using docker
	rm -f *.out *aux *bbl *blg *log *toc *.ptb *.tod *.fls *latexmk *synctex.gz page*.png *dvi *ka *.lof
