# resume

This CV is based on [**Awesome CV**](https://github.com/posquit0/Awesome-CV), a Curriculum Vitae LaTeX template.

The PDF generation is using [Tex Live](https://www.tug.org/texlive/) implementation as a [docker](https://hub.docker.com/r/texlive/texlive).

The PDF retrieval is done by sending it as an email attachment using [mailjet](www.mailjet.com) REST API.

Based on Latex class, Makefile and CI (build stage) by [Romain RICHARD](https://gitlab.com/romaiiiinnn)

## Naming convention

To work properly, the scripts `push.sh` and `send.sh` assume the PDF document name follows this pattern: `cv-??.pdf` including a [two-letter ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).

## Pipeline

```mermaid
graph TB
build-en -- automatic --> push-dropbox
build-fr -- automatic --> push-dropbox
push-dropbox -- manual --> send-mailjet
```

|    Stage     | Description                  |
| :----------: | :--------------------------- |
|   build-en   | Build the English version    |
|   build-fr   | Build the French version     |
| push-dropbox | Push artefacts to DropBox    |
| send-mailjet | Send artefacts using Mailjet |

## Configure DropBox

### DropBox

[Create an account](https://www.dropbox.com), for example choosing the basic free 2-Gb account.

In the [developper settings](https://www.dropbox.com/developers/), open the application console et create a new application with a scoped access into an _app folder_ (single folder created specifically for the app).

For an application named `dummy`, the target DropBox folder will be: `/Applications/dummy`.

Adds the following permissions:

- `files.content.write` (definitely required)
- `files.metadata.write` (possibly required)

Access tokens are transient and expire after some times. Hence it is necessary to get a long-lived refresh token that will be used offline by the script to get a new valid access token.

1. Get an authorization code

Using a browser, open https://www.dropbox.com/oauth2/authorize?client_id=MY_APP_KEY&token_access_type=offline&response_type=code (using the correct value for `MY_APP_KEY`)

Authorize the access and copy the authorization code that is displayed.

2. Get the long-lived regresh code

```bash
APP_KEY=123456789
APP_SECRET=123456789
AUTHORIZATION_CODE=abcdefgh

curl --request POST 'https://api.dropbox.com/oauth2/token' \
     -d "code=$AUTHORIZATION_CODE" \
     -d "grant_type=authorization_code" \
     -u "$APP_KEY:$APP_SECRET"
```

Copy and keep safe the refresh code shown in the JSON result.

### Gitlab

In Settings > CI/CD > Variables, add three protected and masked variables:

|       Variable        | Description                                         |
| :-------------------: | :-------------------------------------------------- |
|    DROPBOX_APP_KEY    | _App key_ in the application settings               |
|  DROPBOX_APP_SECRET   | _App secret_ (use Show) in the application settings |
| DROPBOX_REFRESH_TOKEN | See procedure above                                 |

In the `.gitlab-ci.yml` file, the `push.sh` bash script requires the following arguments in this order :

|         Name          | Description                                   |
| :-------------------: | :-------------------------------------------- |
|    DROPBOX_APP_KEY    | Application key, i.e. `DROPBOX_APP_KEY`       |
|  DROPBOX_APP_SECRET   | Application secret, i.e. `DROPBOX_APP_SECRET` |
| DROPBOX_REFRESH_TOKEN | Refresh token, i.e. `DROPBOX_REFRESH_TOKEN`   |
|      TARGET_PATH      | DropBox target folder                         |
|      WRITE_MODE       | `add` or `overwrite`                          |

## Configure Mailjet

### Mailjet

[Create an account](https://www.mailjet.com/), for example choosing the **Free** plan :

- $0/month
- Forever
- 6,000 emails/month
- 200 emails per day

In Account settings > API Key Management, create your primary API key. The secret key will shown once. The pair `API KEY`/`SECRET KEY` has to be kept safe. It is required to send a mail.

_Note: The provided example has to be modified with your own pair._

### Gitlab

In Settings > CI/CD > Variables, add two protected and masked variables:

|     Variable      | Description                                    |
| :---------------: | :--------------------------------------------- |
| MJ_APIKEY_PUBLIC  | API key in account settings                    |
| MJ_APIKEY_PRIVATE | Provided once during primary API key creation  |
|       EMAIL       | To/from e-mail, e.g. `john.doe@mail.com`       |
|       NAME        | Name associated to the e-mail, e.g. `John Doe` |

In the `.gitlab-ci.yml` file, the `send.sh` bash script requires the following arguments in this order :

|    Name     | Description                                                    |
| :---------: | :------------------------------------------------------------- |
| CREDENTIALS | Mailjet credentials, i.e. `MJ_APIKEY_PUBLIC:MJ_APIKEY_PRIVATE` |
|    EMAIL    | Email associated to Mailjet account                            |
|    NAME     | Name associated to the email                                   |

It is possible to use another email as sender but is has to be legit and be declared first in Mailjet Settings > Senders & Domains > Add a Sender Domain or Address.
