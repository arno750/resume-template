#!/bin/bash

DROPBOX_APP_KEY=$1
DROPBOX_APP_SECRET=$2
DROPBOX_REFRESH_TOKEN=$3
TARGET_PATH=$4
WRITE_MODE=$5

curl --request POST 'https://api.dropbox.com/oauth2/token' \
   -d refresh_token="$DROPBOX_REFRESH_TOKEN" \
   -d grant_type=refresh_token \
   -d client_id="$DROPBOX_APP_KEY" \
   -d client_secret="$DROPBOX_APP_SECRET" \
   --output response.json

ACCESS_TOKEN=`jq -r '.access_token' response.json`

for PDF_FILE in `find . -type f -name 'cv-??.pdf'`
do
    FILENAME=`basename $PDF_FILE`
    echo "Push $FILENAME"
    curl -X POST https://content.dropboxapi.com/2/files/upload \
      --header "Authorization: Bearer $ACCESS_TOKEN" \
      --header "Dropbox-API-Arg: {\"path\": \"$TARGET_PATH/$FILENAME\",\"mode\": \"$WRITE_MODE\",\"autorename\": true,\"mute\": false}" \
      --header "Content-Type: application/octet-stream" \
      --data-binary "@$FILENAME"
done
