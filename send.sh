#!/bin/bash

CREDENTIALS=$1
EMAIL=$2
NAME=$3

MESSAGE="{
    \"Messages\":[
        {
            \"From\": {
                \"Email\": \"$EMAIL\",
                \"Name\": \"$NAME\"
            },
            \"To\": [
                {
                    \"Email\": \"$EMAIL\",
                    \"Name\": \"$NAME\"
                }
            ],
            \"Subject\": \"Latest CV\",
            \"TextPart\": \"Latest CV attached...\",
            \"Attachments\": ["

COMMA=""
for PDF_FILE in `find . -type f -name 'cv-??.pdf'`
do
    FILENAME=`basename $PDF_FILE`
    echo "Send $FILENAME"
    PAYLOAD="`base64 -w 0 $PDF_FILE`"
    MESSAGE="$MESSAGE$COMMA
                {
                    \"ContentType\": \"application/pdf\",
                    \"Filename\": \"$FILENAME\",
                    \"Base64Content\": \"$PAYLOAD\"
                }"
    COMMA=","
done

MESSAGE="$MESSAGE
            ]
        }
    ]
  }"

echo $MESSAGE > message.json

# This call sends a message to the given recipient with attachment.
curl -s \
  -X POST \
  --user "$CREDENTIALS" \
  https://api.mailjet.com/v3.1/send \
  -H 'Content-Type: application/json' \
  -d @message.json